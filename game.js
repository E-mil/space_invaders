var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");

var x, y;
var adown, ddown, spacedown;
var projectiles;
var projectileCounter;
var invaders;
var ixDir;
var iyDir;
var ixTime, iyTime;
var running;
var timer;
const WIDTH = 40, HEIGHT = 40;

addEventListener('keydown', this.onClick, false);
addEventListener('keyup', this.onRelease, false);

initVars();
startScreen();

function initVars () {
	x = 280;
	y = 360;
	adown = false;
	ddown = false;
	spacedown = false;
	projectiles = [];
	projectileCounter = 0;
	invaders = [];
	ixDir = 0;
	iyDir = 1;
	ixTime = 0;
	iyTime = 0;
	running = false;
}

function startScreen () {
	var temp = context.fillStyle;
	context.font = "30px sans-serif";
	context.fillStyle = "red";
	context.fillText("SPACE INVADERS!", 170, 100);
	context.font = "15px sans-serif";
	context.fillStyle = "darkblue";
	context.fillText("Press 'a' to move left     Press 'd' to move right", 150, 220);
	context.fillText("Press 'space' to shoot", 230, 250)
	context.fillText("Press 'space' to start!", 230, 280);
	context.fillStyle = temp;
}

function winScreen () {
	var temp = context.fillStyle;
	context.fillStyle = "red";
	context.font = "15px sans-serif";
	context.fillText("Congratulations", 240, 100);
	context.font = "30px sans-serif";
	context.fillText("YOU WIN!", 225, 150);
	context.font = "15px sans-serif";
	context.fillStyle = "darkblue";
	context.fillText("Press 'space' to play again", 210, 250);
	context.fillStyle = temp;
}

function loseScreen () {
	var temp = context.fillStyle;
	context.fillStyle = "red";
	context.font = "30px sans-serif";
	context.fillText("GAME OVER", 205, 150);
	context.font = "15px sans-serif";
	context.fillStyle = "darkblue";
	context.fillText("Press 'space' to play again", 210, 250);
	context.fillStyle = temp;
}

function addInvaders () {
	for (var j = 0; j < 3; j++) {
		for (var i = 0; i < 9; i++) {
			invaders.push(new invader(new rectangle(30 + 60*i, 20 + 50*j, 30, 30)));
		}
	}
}

function onRelease(e) {
	if (e.keyCode === 65) { //the 'a' button was released
		adown = false;
	} else if (e.keyCode === 68) { //the 'd' button was released
		ddown = false;
	} else if (e.keyCode === 32) {
		spacedown = false;
	}
}

function onClick(e) {
	if (e.keyCode === 65) { //the 'a' button was pressed
		adown = true;
	} else if (e.keyCode === 68) { //the 'd' button was pressed
		ddown = true;
	} else if (e.keyCode === 32) {
		if (!running) {
			initVars();
			running = true;
			addInvaders();
			clear();
			paint();
			timer = setInterval(run, 5);
		} else {
			spacedown = true;
		}
	}
}

function run () {
	clear();
	update();
	if (running) {
		paint();
	}
}

function update () {
	checkCollisions();
	move();
	updateProjectiles();
	updateInvaders();
}

function checkCollisions () {
	for (var i = 0; i < projectiles.length; i++) {
		for (var j = 0; j < invaders.length; j++) {
			if (projectiles[i].rect.intersects(invaders[j].rect)) {
				projectiles.splice(i, 1);
				invaders.splice(j, 1);	
				i--;
				j--;
				break;
			}
		}
	}
}

function updateProjectiles () {
	--projectileCounter;
	if (spacedown && projectileCounter < 1) {
		projectiles.push(new projectile(new rectangle(x + 17, y - 3, 6, 6)));
		projectileCounter = 100;
	}

	var temp = [];
	for (var i = 0; i < projectiles.length; i++) {
		if (projectiles[i].update())
			temp.push(projectiles[i]);
	}
	projectiles = temp;
}

function updateInvaders() {
	if (ixDir !== 0) {
		ixTime--;
	} else {
		iyTime--;
	}

	if (ixTime < 0) {
		iyDir = ixDir;
		ixDir = 0;
		iyTime = 60;
		ixTime = 0;
	} else if (iyTime < 0) {
		ixDir = iyDir;
		iyDir = 0;
		ixTime = 400;
		iyTime = 0;
	}

	var temp = 1;
	for (var i = 0; i < invaders.length; i++) {
		var asd = invaders[i].update(ixDir, Math.abs(iyDir));
		if (asd === 0) {
			temp = -1;
		} else if (asd === 1) {
			//Game is over
			running = false;
			clearInterval(timer);
			loseScreen();
		}
	}
	ixDir *= temp;

	//Game is won
	if (invaders.length === 0) {
		running = false;
		clearInterval(timer);
		winScreen();
	}
}

function clear() {
	context.clearRect(0, 0, 600, 400);
}

function move() {
	if (x < 0) {
		x = 0;
	} else if (x > 600 - WIDTH) {
		x = 600 - WIDTH;
	}
	if (adown) {
		x -= 1.5;
	}
	if (ddown) {
		x += 1.5;
	}
}

function paint() {
	context.fillRect(x, y, WIDTH, HEIGHT);

	for (var i = 0; i < projectiles.length; i++) {
		projectiles[i].draw(context);
	}

	for (var i = 0; i < invaders.length; i++) {
		invaders[i].draw(context);
	}
}