class rectangle {
	constructor (x, y, width, height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	//returns true if rectangle intersects this, else false
	intersects (rectangle) {
		if (this.x > rectangle.x + rectangle.width || this.x + this.width < rectangle.x || this.y > rectangle.y + rectangle.height || this.y + this.height < rectangle.y) {
			return false;
		}
		return true;
	}
}