class invader {
	constructor (rect) {
		this.rect = rect;
		this.SPEED = 0.3;
		this.x = 1;
		this.y = 0;
	}

	update (x, y) {
		this.rect.x += this.SPEED * x;
		this.rect.y += this.SPEED * y;
		if (this.rect.x + this.rect.width > 600 || this.rect.x < 0) {
			return 0;
		}
		if (this.rect.y + this.rect.height > 365)  {
			return 1;
		}
		return 2;
		/*if (this.x !== 0) {
			this.rect.x += this.x * this.SPEED;
			if (this.rect.x + this.rect.width > 600 || this.rect.x < 0) {
				this.x = 0;
				this.y = this.rect.y;
			}
		} else {
			this.rect.y += this.SPEED;
			if (this.y + 40 < this.rect.y) {
				if (this.rect.x < 0) {
					this.x = 1;
				} else {
					this.x = -1;
				}
			}
		}*/
	}

	draw (context) {
		var temp = context.fillStyle;
		context.fillStyle = "darkgreen";
		context.fillRect(this.rect.x, this.rect.y, this.rect.width, this.rect.height);
		context.fillStyle = temp;
	}
}