
class projectile {

	constructor (rect) {
		this.rect = rect;
		this.SPEED = 1.5;
	}

	update() {
		 this.rect.y -= this.SPEED;
		 if (this.rect.y < 0)
		 	return false;
		 return true;
	}

	draw(context) {
		var temp = context.fillStyle;
		context.fillStyle = "red";
		context.fillRect(this.rect.x, this.rect.y, this.rect.width, this.rect.height);
		context.fillStyle = temp;
	}
}